container = d3.select ig.containers.base
mapElement = container.append \div
  ..attr \id \map
map = L.map do
  * mapElement.node!
  * minZoom: 6,
    maxZoom: 14,
    zoom: 7,
    center: [49.78, 15.5]
    maxBounds: [[48.3,11.6], [51.3,19.1]]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l1/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75

layers =
  ["crv_celkem"          "Všechna vozidla" "vozidel" "celkem"]
  ["crv_osobni"          "Osobní vozy" "osobních vozů" "osobni"]
  ["crv_terenni"         "Terénní vozy" "terénních vozů" "terenni"]
  ["crv_uzitkove"        "Užitkové vozy" "užitkových vozů" "uzitkovy"]
  ["male_motorky"         "Malé motocykly" "malých motocyklů" "malymotocykl"]
  ["crv_velkymotocykl"   "Velké motocykly" "velkých motocyklů" "velkymotocykl"]
  ["crv_autobusy"        "Autobusy" "autobusů" "autobus"]
  ["crv_nakladni"        "Nákladní vozy" "nákladních vozů" "nakladni"]
  ["crv_tahac"           "Tahače" "tahačů" "tahac"]
  ["traktory"            "Traktory" "traktorů" "traktor"]
  ["crv_veteranautomoto" "Veteráni" "veteránů" "veteranautomoto"]
  ["crv_zavodniautomoto" "Závodní vozy" "závodních vozů" "zavodniautomoto"]
  ["crv_sneznyskutr"     "Sněžné skútry" "sněžných skútrů" "snezyskutr"]

layers_assoc = {}
layer_meta = {}
for [id, name, plural, property]:meta in layers
  layer = L.tileLayer.betterWms do
    * "https://samizdat.cz/proxy/smz_map/wms"
    * layers: 'opengeo:druhy_vozidel_obce_poly_webmerc'
      format: 'image/png'
      styles: id
      zIndex: 2
      opacity: 0.8
  layers_assoc[name] = layer
  layer_meta[name] = meta

layerControl = L.control.layers do
  layers_assoc
  {}
  collapsed: no
  autoZIndex: no



layerControl
  ..addTo map
currentMeta = null
currentLayer = null
map.on \baselayerchange (layer) ->
  currentLayer := layer.layer
  currentMeta := layer_meta[layer.name]

map
  ..addLayer baseLayer
  ..addLayer layers_assoc["Všechna vozidla"]
  ..addLayer labelLayer
popup = L.popup!
map.on \click (evt) ->
  url = currentLayer.getFeatureInfoUrl evt.latlng
  (err, data) <~ d3.json url
  return if err
  return unless data.features.length
  {properties} = data.features.0
  console.log properties
  text = "<b>#{properties.obec}</b><br>"
  rate = properties[currentMeta.3 + '_obyv']
  num = rate * properties['obyv']
  text += "V této obci je <b>#{ig.utils.formatNumber num}</b> #{currentMeta.2} a #{ig.utils.formatNumber properties['obyv']} obyvatel, tedy <b>#{ig.utils.formatNumber rate * 1000, 1}</b> na 1000 obyvatel"
  popup
    ..setLatLng evt.latlng
    ..setContent text
    ..openOn map


geocoder = new ig.Geocoder ig.containers.base
  ..on \latLng (latlng) ->
    map.setView latlng, 12
